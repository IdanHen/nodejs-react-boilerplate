const path = require('path');
const paths = require('./paths');

module.exports = {
    resolve: {
        modules: ['node_modules', paths.appSrc],
        alias: {
            // 'ag-grid': path.resolve('./node_modules/ag-grid')
        },
    },
    module: {
        // rules: [{
        //     test: /\.less$/,
        //     use: [{
        //         loader: "style-loader" // creates style nodes from JS strings
        //     }, {
        //         loader: "css-loader" // translates CSS into CommonJS
        //     }, {
        //         loader: "less-loader" // compiles Less to CSS
        //     }]
        // }]
    }
};