import React, { Component } from 'react';
import 'App.css';
import {Switch, Route, HashRouter, Redirect} from "react-router-dom";
import routes from "constants/routes";

class App extends Component {
  render() {
    return (
        <HashRouter>
            <Switch>
                <Route  path={routes.login.path} restriction="isGuest" component={routes.login.component}/>
                {/*<Route  path={routes.layout.path} restriction="isAuthenticated" component={routes.layout.component}/>*/}
                {/*<Route  restriction="isGuest" component={routes.login.component}/>*/}
                <Redirect to={routes.login.path} />
                {/*<div>asd</div>*/}

            </Switch>
        </HashRouter>
    );
  }
}

export default App;
