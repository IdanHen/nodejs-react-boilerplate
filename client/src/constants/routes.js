import Login from "views/login/Login";

export default {
    login: {
        path: "/login",
        component: Login
    }
};