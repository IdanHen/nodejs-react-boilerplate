Purchaser Project: Server & Client


## Installation
--
First Run: 
1. run ```sudo npm install -g nodemon``` to install nodemon on your system
1. run ```npm install``` in the main folder
2. run ``` npm install ``` in client folder

#### Available Commands
-
* ```npm run dev``` - will start both client & server , with watchers.
* ```npm run build:production``` - will create a production ready client bundle

#### URLS
--
* http://localhost:9000/  (Server)
* http://localhost:3000/  (Client)